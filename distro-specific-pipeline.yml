stages:
  - get_base_image
  - create_build_instance
  - instance_setup
  - snapshot_build_instance
  - download_and_clean_final_image
  - upload_final_image_for_testing
  - create_test_instance
  - instance_test
  - upload_and_publish_final_image
  - publish_iu_image

get_base_image:
  stage: get_base_image
  retry: 2
  before_script:
    - command -v qemu-img || sudo apt-get install -y qemu-utils
    - command -v openstack || sudo apt-get install -y python3-openstackclient
    - source $JETSTREAM2_APP_CREDENTIAL_OPENRC
  script:
    - wget --no-verbose $IMAGE_DOWNLOAD_URL -O image.qcow2
    # Grow the root partition of RHEL-variant images
    - >
      if [ "$DISTRO_NAME" = "almalinux" ]; then
        qemu-img create -f qcow2 -o size=12G,preallocation=metadata outdisk.qcow2
        sudo virt-resize --resize /dev/sda3=1536M --expand /dev/sda4 image.qcow2 outdisk.qcow2
        mv -f outdisk.qcow2 image.qcow2
      fi
    - >
      if [ "$DISTRO_NAME" = "rocky" ]; then
        qemu-img create -f qcow2 -o size=12G,preallocation=metadata outdisk.qcow2
        sudo virt-resize --resize /dev/sda2=1536M --expand /dev/sda5 image.qcow2 outdisk.qcow2
        mv -f outdisk.qcow2 image.qcow2
      fi
    - TIMESTAMP="$(date +%s%N)"
    - >
      BASE_IMAGE_UUID="$(
      openstack image create
      --file image.qcow2
      --disk-format qcow2
      --container-format bare
      --property skip_atmosphere=yes
      --property hw_disk_bus=scsi
      --property hw_scsi_model=virtio-scsi
      --property hw_qemu_guest_agent=yes
      --property os_require_quiesce=yes
      --property hw_machine_type=q35
      --private
      --format value
      --column id
      base-image-${DISTRO_VAR}-$TIMESTAMP
      )"
    - '[[ "${UEFI_ENABLED_DISTROS[*]}" =~ "$DISTRO_VAR" ]] && openstack image set --property hw_firmware_type=uefi $BASE_IMAGE_UUID'
    - echo "BASE_IMAGE_UUID=$BASE_IMAGE_UUID" | tee -a get_base_image.env
  artifacts:
    reports:
      dotenv: get_base_image.env

create_build_instance:
  stage: create_build_instance
  retry: 2
  before_script:
    - source $JETSTREAM2_APP_CREDENTIAL_OPENRC
  script:
    - TIMESTAMP="$(date +%s%N)"
    # TODO don't hard-code security group
    - >
      BUILD_INSTANCE_UUID_EXTRA_NEWLINE="$(
      openstack server create
      --image $BASE_IMAGE_UUID
      --flavor ${BUILD_FLAVOR}
      --network auto_allocated_network
      --key-name image-build-pipeline
      --security-group exosphere
      --wait
      --format value
      --column id
      image-build-instance-${DISTRO_VAR}-$TIMESTAMP
      )"
    - BUILD_INSTANCE_UUID="$(echo $BUILD_INSTANCE_UUID_EXTRA_NEWLINE | tr -d '\n')"
    - echo "BUILD_INSTANCE_UUID=$BUILD_INSTANCE_UUID" | tee -a create_build_instance.env
    # TODO don't hard-code network
    - >
      BUILD_INSTANCE_FLOATING_IP="$(
      openstack floating ip create
      --format value
      --column floating_ip_address
      public
      )"
    - echo "BUILD_INSTANCE_FLOATING_IP=$BUILD_INSTANCE_FLOATING_IP" | tee -a create_build_instance.env
    - >
      openstack server add floating ip $BUILD_INSTANCE_UUID $BUILD_INSTANCE_FLOATING_IP
  artifacts:
    reports:
      dotenv: create_build_instance.env


instance_setup:
  stage: instance_setup
  retry: 2
  before_script:
    - command -v ansible || sudo add-apt-repository --yes --update ppa:ansible/ansible
    - command -v ansible || sudo apt-get install -y ansible
    - ansible-galaxy collection install community.general
    - ansible-galaxy collection install ansible.posix
  script:
    - echo $DISTRO_VAR
    # Unsure if this is needed
    - chmod 400 $SSH_PRIV_KEY
    - cd ansible
    - >
      ansible-playbook
      --inventory "$(echo $BUILD_INSTANCE_FLOATING_IP)",
      --user $DISTRO_NAME
      --private-key $SSH_PRIV_KEY
      --become
      setup.yml

snapshot_build_instance:
  stage: snapshot_build_instance
  retry: 2
  before_script:
    - source $JETSTREAM2_APP_CREDENTIAL_OPENRC
  script:
    # Wait until instance is powered off
    - |
      while :
      do
          STATUS="$(openstack server show --format value --column status $BUILD_INSTANCE_UUID)"
          echo $STATUS
          if  [ "$STATUS" == "SHUTOFF" ]
          then
              break
          fi
          sleep 5
      done
    - TIMESTAMP="$(date +%s%N)"
    - >
      MODIFIED_IMAGE_UUID="$(
      openstack server image create
      --name modified-image-${DISTRO_VAR}-$TIMESTAMP
      --format value
      --column id
      $BUILD_INSTANCE_UUID
      )"
    # poll and wait until image is active
    - |
      while :
      do
          MODIFIED_IMAGE_STATUS="$(openstack image show --format value --column status $MODIFIED_IMAGE_UUID)"
          echo $MODIFIED_IMAGE_STATUS
          if  [ "$MODIFIED_IMAGE_STATUS" == "active" ]
          then
              break
          fi
          sleep 5
      done
    - echo "MODIFIED_IMAGE_UUID=$MODIFIED_IMAGE_UUID" | tee -a snapshot_build_instance.env
  artifacts:
    reports:
      dotenv: snapshot_build_instance.env

download_and_clean_final_image:
  stage: download_and_clean_final_image
  retry: 2
  before_script:
    - source $JETSTREAM2_APP_CREDENTIAL_OPENRC
  script:
    # Begin angry hot patch for https://review.opendev.org/c/openstack/python-openstackclient/+/763317
    # Probably won't be needed someday

    - >
      sudo sed
      -i
      's/image_client.download_image(image.id, output=parsed_args.file)/image_client.download_image(image.id, stream=True, output=parsed_args.file)/g'
      /usr/lib/python3/dist-packages/openstackclient/image/v1/image.py
    - >
      sudo sed
      -i
      's/image_client.download_image(image.id, output=parsed_args.file)/image_client.download_image(image.id, stream=True, output=parsed_args.file)/g'
      /usr/lib/python3/dist-packages/openstackclient/image/v2/image.py
    # End angry hot patch
    - openstack image save --file /home/gitlab-runner/${DISTRO_VAR}.raw $MODIFIED_IMAGE_UUID
    - >
      sudo virt-sysprep
      --format raw
      -a /home/gitlab-runner/${DISTRO_VAR}.raw

upload_final_image_for_testing:
  stage: upload_final_image_for_testing
  retry: 2
  before_script:
    - source $JETSTREAM2_APP_CREDENTIAL_OPENRC
  script:
    # Upload to Jetstream2
    - TIMESTAMP="$(date +%s%N)"
    - IMAGE_NAME=${FINAL_IMAGE_NAME_JETSTREAM2}-$CI_COMMIT_BRANCH-$TIMESTAMP
    - >
      FINAL_IMAGE_UUID_JETSTREAM2="$(
      openstack image create
      --file /home/gitlab-runner/${DISTRO_VAR}.raw
      --disk-format raw
      --container-format bare
      --property hw_disk_bus=scsi
      --property hw_scsi_model=virtio-scsi
      --property hw_qemu_guest_agent=yes
      --property os_require_quiesce=yes
      --property hw_machine_type=q35
      --private
      --format value
      --column id
      ${IMAGE_NAME}
      )"
    - '[[ "${UEFI_ENABLED_DISTROS[*]}" =~ "$DISTRO_VAR" ]] && openstack image set --property hw_firmware_type=uefi $FINAL_IMAGE_UUID_JETSTREAM2'
    - echo "FINAL_IMAGE_UUID_JETSTREAM2=$FINAL_IMAGE_UUID_JETSTREAM2" | tee -a clean_final_image.env
  artifacts:
    reports:
      dotenv: clean_final_image.env


create_test_instance:
  stage: create_test_instance
  retry: 2
  before_script:
    - source $JETSTREAM2_APP_CREDENTIAL_OPENRC
  script:
    - TIMESTAMP="$(date +%s%N)"
    - >
      TEST_INSTANCE_UUID_EXTRA_NEWLINE="$(
      openstack server create
      --image $FINAL_IMAGE_UUID_JETSTREAM2
      --flavor m3.quad
      --network auto_allocated_network
      --key-name image-build-pipeline
      --security-group exosphere
      --wait
      --format value
      --column id
      image-test-instance-$TIMESTAMP
      )"
    - TEST_INSTANCE_UUID="$(echo $TEST_INSTANCE_UUID_EXTRA_NEWLINE | tr -d '\n')"
    - echo "TEST_INSTANCE_UUID=$TEST_INSTANCE_UUID" | tee -a create_test_instance.env
    # TODO don't hard-code network
    - >
      TEST_INSTANCE_FLOATING_IP="$(
      openstack floating ip create
      --format value
      --column floating_ip_address
      public
      )"
    - echo "TEST_INSTANCE_FLOATING_IP=$TEST_INSTANCE_FLOATING_IP" | tee -a create_test_instance.env
    - >
      openstack server add floating ip $TEST_INSTANCE_UUID $TEST_INSTANCE_FLOATING_IP
  artifacts:
    reports:
      dotenv: create_test_instance.env

instance_test:
  stage: instance_test
  retry: 2
  before_script:
    - command -v ansible || sudo apt-get install -y ansible
    - ansible-galaxy collection install community.general
  script:
    # Unsure if this is needed
    - chmod 400 $SSH_PRIV_KEY
    - cd ansible
    - >
      ansible-playbook
      --inventory "$(echo $TEST_INSTANCE_FLOATING_IP)",
      --user $DISTRO_NAME
      --private-key $SSH_PRIV_KEY
      --become
      test.yml

.upload_and_publish_final_image:
  rules:
     - if: $CI_COMMIT_BRANCH == "main"
  stage: upload_and_publish_final_image
  retry: 2
  allow_failure: true
  script:
    - export OS_REGION_NAME=${REGION}
    - TIMESTAMP="$(date +%s%N)"
    - TEMP_IMAGE_NAME=${FINAL_IMAGE_NAME_JETSTREAM2}-$CI_COMMIT_BRANCH-$TIMESTAMP
    - >
      openstack image create
      --file /home/gitlab-runner/${DISTRO_VAR}.raw
      --disk-format raw
      --container-format bare
      --property hw_disk_bus=scsi
      --property hw_scsi_model=virtio-scsi
      --property hw_qemu_guest_agent=yes
      --property os_require_quiesce=yes
      --property hw_machine_type=q35
      --public
      --id ${FINAL_IMAGE_UUID_JETSTREAM2}
      ${TEMP_IMAGE_NAME}
    - '[[ "${UEFI_ENABLED_DISTROS[*]}" =~ "$DISTRO_VAR" ]] && openstack image set --property hw_firmware_type=uefi $FINAL_IMAGE_UUID_JETSTREAM2'
    - |
      if openstack image show ${FINAL_IMAGE_NAME_JETSTREAM2};
      then
        openstack image set --deactivate --community --name ${FINAL_IMAGE_NAME_JETSTREAM2}-deprecated-$TIMESTAMP ${FINAL_IMAGE_NAME_JETSTREAM2}
      fi
    - openstack image set --name ${FINAL_IMAGE_NAME_JETSTREAM2} ${TEMP_IMAGE_NAME}

upload_and_publish_final_image_to_satellites:
  extends: .upload_and_publish_final_image
  before_script:
    - source $JETSTREAM2_APP_CREDENTIAL_OPENRC
  parallel:
    matrix:
      - REGION: UH
      - REGION: ASU
      - REGION: TACC
      - REGION: Cornell

upload_and_publish_final_image_to_rescloud:
  extends: .upload_and_publish_final_image
  before_script:
    - source $RESCLOUD_APP_CREDENTIAL_OPENRC
  variables:
    REGION: "RegionOne"
  rules:
    - if: $PUBLISH_TO_RESCLOUD == "true" && $CI_COMMIT_BRANCH == "main"

publish_iu_image:
  only:
    - main
  stage: publish_iu_image
  retry: 2
  before_script:
    - source $JETSTREAM2_APP_CREDENTIAL_OPENRC
  script:
    - TIMESTAMP="$(date +%s%N)"
    # Deprecate old image
    - >
      openstack image set
      --name ${FINAL_IMAGE_NAME_JETSTREAM2}-deprecated-$TIMESTAMP
      --deactivate
      --community
      $FINAL_IMAGE_NAME_JETSTREAM2
    # Promote new image
    - >
      openstack image set
      --name $FINAL_IMAGE_NAME_JETSTREAM2
      --public
      $FINAL_IMAGE_UUID_JETSTREAM2

delete_temp_resources:
  stage: .post
  retry: 2
  rules:
    - when: always
  before_script:
    - source $JETSTREAM2_APP_CREDENTIAL_OPENRC
  script:
    - rm /home/gitlab-runner/${DISTRO_VAR}.raw
    - openstack server delete $BUILD_INSTANCE_UUID || true
    - openstack floating ip delete $BUILD_INSTANCE_FLOATING_IP || true
    - openstack server delete $TEST_INSTANCE_UUID || true
    - openstack floating ip delete $TEST_INSTANCE_FLOATING_IP || true
    - openstack image delete $BASE_IMAGE_UUID || true
    - openstack image delete $MODIFIED_IMAGE_UUID || true
