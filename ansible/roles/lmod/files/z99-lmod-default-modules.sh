#!/bin/sh

# Per https://lmod.readthedocs.io/en/latest/070_standard_modules.html
if [ -z "$__Init_Default_Modules" ]; then
   export __Init_Default_Modules=1;

   export LMOD_SYSTEM_DEFAULT_MODULES="xalt"
   module --initial_load --no_redirect restore
else
   module refresh
fi